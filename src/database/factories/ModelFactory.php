<?php
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/**
 * Permission
 */
$factory->define(\Webmagic\Users\Models\Permission::class, function (Faker\Generator $faker) {
    return [
        'name' => $name = $faker->word,
        'slug' => str_slug($name, '-')
    ];
});


/**
 * Role
 */
$factory->define(\Webmagic\Users\Models\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => $name = $faker->word,
        'slug' => str_slug($name, '-')
    ];
});


/**
 * User
 */
$factory->define(\Webmagic\Users\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $name = $faker->word,
        'password' => $faker->password(6, 16)
    ];
});