<div class="form-group">
    {!! $form_builder->label(__('users::common.role-name'), null) !!}
    {!! $form_builder->text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! $form_builder->label(__('users::common.role-tag'), null) !!}
    {!! $form_builder->text('slug', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! $form_builder->label(__('users::common.description'), null) !!}
    {!! $form_builder->textarea('description', null, ['class'=>'form-control']) !!}
</div>
{{--<div class="form-group">--}}
    {{--{!! $form_builder->label('Уровень прав', null) !!}--}}
    {{--{!! $form_builder->text('level', 1, ['class' => 'form-control']) !!}--}}
{{--</div>--}}
<hr>
<h4>@lang('users::common.permission')</h4>
@foreach($permissions as $permission)
    <div class="form-group">
        {!! $form_builder->checkbox('permissions[' . $permission['slug'] . ']', null, isset($attached_permissions[$permission['slug']])) !!}
        {!! $form_builder->label($permission['name'], null) !!} (<span class="text-green">{{$permission['slug']}}</span>)
    </div>
@endforeach
<div class="box-footer">
    <button type="submit" class="btn btn-primary"><span class="fa fa-save"> </span> @lang('users::common.save')</button>
</div>
