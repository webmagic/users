<h1>@lang('users::common.role-edition')</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model($role,['url' => route('users_module::roles::update', $role['id']), 'class' => 'js-submit', 'method' => 'PUT']) !!}
                    @include('users::roles._form')
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>

