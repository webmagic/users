<h1>@lang('users::common.available-roles')</h1>
    <div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <a class="btn btn-sm btn-success pull-right"
                   href="{{ route('users_module::roles::create') }}"
                   data-action="{{ route('users_module::roles::create') }}">
                    <i class="fa  fa-plus"> </i> @lang('users::common.add')
                </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-hover table-bordered">
                    <tbody><tr>
                        <th>@lang('users::common.id')</th>
                        <th>@lang('users::common.role-name')</th>
                        <th>@lang('users::common.role')</th>
                        <th>@lang('users::common.description')</th>
                        <th></th>
                    </tr>
                    @foreach($roles as $role)
                        <tr class="js_role_{{$role['id']}}">
                            <td>{{$role['id']}}</td>
                            <td>{{$role['name']}}</td>
                            <td><span class="text-light-blue">{{$role['slug']}}</span></td>
                            <td>{{$role['description']}}</td>
                            <td width="100px">
                                <div class="btn-group">
                                    <a href="{{ route('users_module::roles::edit', $role['id']) }}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a data-item=".js_role_{{$role['id']}}" data-method="POST"
                                               data-request="{{ route('users_module::roles::destroy', $role['id']) }}"
                                               class="js_delete products-delete">@lang('users::common.delete')
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    </div>
