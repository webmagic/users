<h1>@lang('users::common.user-creation')</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model($user, ['url' => route('users_module::users::store'), 'class' => 'js-submit']) !!}
                    @include('users::_form')
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>
