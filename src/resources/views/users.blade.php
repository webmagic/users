<h1>{{trans('users::common.users')}}</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-sm btn-success pull-right"
                            href="{{ route('users_module::users::create') }}">
                        <i class="fa  fa-plus"> </i> {{trans('users::common.add')}}
                    </a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover  table-bordered">
                                <tbody>
                                <tr>
                                    <th>@lang('users::common.id')</th>
                                    <th>@lang('users::common.login')</th>
                                    <th>@lang('users::common.roles')</th>
                                    <th></th>
                                </tr>
                                @foreach($users as $user)
                                    <tr class="js_user_{{$user['id']}}">
                                        <td>{{$user['id']}}</td>
                                        <td>{{$user['name']}}</td>
                                        <td>@if(isset($user->roles[0]))
                                                @foreach($user->roles as $role)
                                                    {{$role['name']}} <br>
                                                @endforeach
                                            @else - @endif</td>
                                        <td width="100px">
                                            <div class="btn-group">
                                                <a class="btn btn-info" href="{{ route('users_module::users::edit', $user['id']) }}"><i class="fa fa-pencil-square-o"></i></a>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a data-item=".js_user_{{$user['id']}}" data-method="POST"
                                                           data-request="{{ route('users_module::users::destroy', $user['id']) }}"
                                                           class="js_delete products-delete">@lang('users::common.delete')</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
