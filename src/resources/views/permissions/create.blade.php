<h1>@lang('users::common.permissions-creation')</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model($permission, ['url' => route('users_module::permissions::store'), 'class' => 'js-submit']) !!}
                    @include('users::permissions._form')
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>

