<h1>@lang('users::common.permissions')</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-sm btn-success pull-right"
                       href="{{ route('users_module::permissions::create') }}"
                       data-action="{{ route('users_module::permissions::create') }}">
                        <i class="fa  fa-plus"> </i> @lang('users::common.add')
                    </a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>@lang('users::common.id')</th>
                                    <th>@lang('users::common.permissions')</th>
                                    <th>@lang('users::common.permission-tag')</th>
                                    <th>@lang('users::common.description')</th>
                                    <th width="70px"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permissions as $permission)
                                    <tr class="js_permission_{{$permission['id']}}">
                                        <td>{{$permission['id']}}</td>
                                        <td>{{$permission['name']}}</td>
                                        <td><span class="text-green">{{$permission['slug']}}</span></td>
                                        <td>{{$permission['description']}}</td>
                                        <td width="100px">
                                            <div class="btn-group">
                                                <a href="{{ route('users_module::permissions::edit', $permission['id']) }}"
                                                   class="btn btn-info"
                                                   data-action="{{ route('users_module::permissions::edit', $permission['id']) }}">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                </a>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a data-item=".js_permission_{{$permission['id']}}" data-method="POST"
                                                           data-request="{{ route('users_module::permissions::destroy', $permission['id']) }}"
                                                           class="js_delete products-delete">@lang('users::common.delete')</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
