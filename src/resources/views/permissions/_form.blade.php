
{!! $form_builder->label(__('users::common.title'), null) !!}
{!! $form_builder->text('name', null, ['class' => 'form-control']) !!}
{!! $form_builder->label(__('users::common.permission-tag'), null) !!}
{!! $form_builder->text('slug', null, ['class' => 'form-control']) !!}
{!! $form_builder->label(__('users::common.description'), null) !!}
{!! $form_builder->textarea('description', null, ['class'=>'form-control']) !!}

<!-- /.box-body -->
<div class="box-footer">
    <button type="submit" class="btn btn-primary"><span class=""> </span> @lang('users::common.save')</button>
</div>
