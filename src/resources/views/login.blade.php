@extends('dashboard::core.adminlte_base')

@section('body_class') @parent hold-transition login-page @endsection

@section('base_content')
<div class="login-box">
    <div class="login-logo">
        <a href="{{url(config('webmagic.dashboard.users.login_logo_rout'))}}">
            {{config('webmagic.dashboard.users.login_logo_name')}}
        </a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">

        @if(count($errors))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>@lang('users::common.errors')</h4>
                @foreach($errors->all() as $error)
                    <p>{{$error}}</p>
                @endforeach
            </div>
        @endif
        <p class="login-box-msg">@lang('users::common.login-message')</p>

            {!! $form_builder->open(['url' => route('login')]) !!}
            <div class="form-group has-feedback">
                {!! $form_builder->text('name', old('name'), ['class' => 'form-control', 'placeholder' => __('users::common.login')]) !!}
                <span class="fa fa-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                {!! $form_builder->password('password', ['class' => 'form-control', 'placeholder' => __('users::common.password')]) !!}
            </div>
            <div class="row">
                <div class="col-xs-8">

                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">@lang('login-btn')</button>
                </div>
                <!-- /.col -->
            </div>
        {!! $form_builder->close() !!}
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<script src="{{asset('webmagic/dashboard/js/libs.js')}}"></script>
<script src="{{asset('webmagic/dashboard/js/script.js')}}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
@endsection
