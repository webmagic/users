<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Common translation for users
    |--------------------------------------------------------------------------
    |
    */

    'errors' => 'Errors:',
    'login-message' => 'Login to get started',
    'login' => 'login',
    'password' => 'password',
    'login-bt' => 'login',

    'confirm-password' => 'Confirm password',
    'add' => 'Add',
    'id' => 'ID',
    'login-btn' => 'Submit',
    'role' => 'Role',
    'roles' => 'Roles',
    'delete' => 'Delete',
    'users' => 'Users',
    'additional-permissions' => 'Additional permissions',
    'save' => 'Save',
    'user-creation' => 'User creation',
    'user-edition' => 'User edition',
    'available-roles' => 'Available roles',
    'role-name' => 'Name',
    'description' => 'Description',
    'role-edition' => 'Role edition',
    'role-creation' => 'Role creation',
    'role-tag' => 'Role tag',
    'permissions' => 'Permissions',
    'permission-name' => 'Name',
    'permission-tag' => 'Permission tag',
    'permissions-edition' => 'Permission edition',
    'permissions-creation' => 'Permission creation',
    'title' => 'Title'
];