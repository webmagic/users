<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Common translation for users
    |--------------------------------------------------------------------------
    |
    */

    'login' => 'Логин',
    'password' => 'Пароль',
    'confirm-password' => 'Подтвердите пароль',
    'errors' => 'Ошибки:',
    'login-message' => 'Войдите для начала работы',
    'add' => 'Добавить',
    'id' => 'ID',
    'login-btn' => 'Войти',
    'role' => 'Роль',
    'roles' => 'Роли',
    'delete' => 'Удалить',
    'users' => 'Пользователи',
    'additional-permissions' => 'Дополнительные права',
    'save' => 'Сохранить',
    'user-creation' => 'Создание пользователя',
    'user-edition' => 'Редактирование пользователя',
    'available-roles' => 'Доступные роли',
    'role-name' => 'Название',
    'description' => 'Описание',
    'role-edition' => 'Редактирование роли',
    'role-creation' => 'Создание роли',
    'role-tag' => 'Тип роли (tag)',
    'permissions' => 'Права',
    'permission-name' => 'Права',
    'permission-tag' => 'Тип прав (tag)',
    'permissions-edition' => 'Редактирование прав',
    'permissions-creation' => 'Создание прав',
    'title' => 'Название'
];