<?php

namespace Webmagic\Users;

use Illuminate\Routing\Router;
use Webmagic\Dashboard\Dashboard;

class UserDashboardServiceProvider extends UserServiceProvider
{
    /**
     * Register User dashboard services provider
     */
    public function register()
    {
        parent::register();

        $this->mergeConfigFrom(
            __DIR__.'/config/users_dashboard.php', 'webmagic.dashboard.users'
        );
    }

    /**
     * Register User dashboard routes
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        include 'Http/routes_dashboard.php';

        //Load Views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'users');

        $this->loadTranslations();

        //Config publishing
        $this->publishes([
            __DIR__.'/config/users_dashboard.php' => config_path('webmagic/dashboard/users.php'),
        ], 'config');

        //Views publishing
        $this->publishes([
            __DIR__.'/resources/views' => base_path('/resources/views/vendor/users/'),
        ], 'views');

        //Add items menu
        $this->includeMenuForDashboard();
    }

    /**
     * Including menu items for new dashboard
     */
    protected function includeMenuForDashboard()
    {
        $menu_item_config = config('webmagic.dashboard.users.dashboard_menu_item');
        $dashboard = $this->app->make(Dashboard::class);
        $dashboard->getMainMenu()->addMenuItems($menu_item_config);
    }

    /**
     * Load translation
     */
    protected function loadTranslations()
    {
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'users');
    }

}