<?php

return [

    /*
  |--------------------------------------------------------------------------
  | Name and url for login page
  |--------------------------------------------------------------------------
  */
    'login_logo_rout' => '/',
    'login_logo_name' => 'Site name',

    /*
    |--------------------------------------------------------------------------
    | Module routes prefix
    |--------------------------------------------------------------------------
    |
    | This prefix use for generation all routes for module
    |
    */

    'prefix' => 'dashboard/users',

    /*
    |--------------------------------------------------------------------------
    | Middleware
    |--------------------------------------------------------------------------
    |
    | Can use middleware for access to module page
    |
    */

    'middleware' => ['users', 'auth'],

    /*
    |--------------------------------------------------------------------------
    | Parent category in dashboard
    |--------------------------------------------------------------------------
    |
    | Use for generation module page in dashboard.
    | Use '' if not need parent category
    |
    */

    'menu_parent_category' => '',

    /*
    |--------------------------------------------------------------------------
    | Dashboard menu item config
    |--------------------------------------------------------------------------
    |
    | Config new item in dashboard menu
    |
    */

    'menu_item_name' => 'users',

    'dashboard_menu_item' => [
        'link' => 'dashboard/users/user',
        'text' => 'users::common.users',
        'icon' => 'fa-user',
        'subitems' => [
             [
                'link' => 'dashboard/users/user',
                'text' => 'users::common.users',
                'icon' => 'fa-user',
                 'active_rules' => [
                     'routes_parts'=>[
                         'users_module::users::'
                     ]
                 ]
            ],
            [
                'link' => 'dashboard/users/permissions',
                'text' => 'users::common.permissions',
                'icon' => 'fa-male',
                'active_rules' => [
                    'routes_parts'=>[
                        'users_module::permissions::'
                    ]
                ]
            ],
            [
                'link' => 'dashboard/users/roles',
                'text' => 'users::common.roles',
                'icon' => 'fa-black-tie',
                'active_rules' => [
                    'routes_parts'=>[
                        'users_module::roles::'
                    ]
                ]
            ],

        ]
    ]
];
