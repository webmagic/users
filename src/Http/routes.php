<?php

/*
 * Auth routs
 *
 */

Route::group([
    'middleware' => 'users',
    'namespace' => '\Webmagic\Users\Http\Controllers\Auth'
    ], function () {

    Route::get('login', [
        'as' => 'login',
        'uses' => 'AuthController@showLoginForm'
    ]);
    Route::post('login', [
        'as' => '',
        'uses' => 'AuthController@login'
    ]);
    Route::get('logout', [
        'as' => 'logout',
        'uses' => 'AuthController@logout'
    ]);
});

