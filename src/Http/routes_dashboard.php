<?php

Route::group([
        'prefix' => config('webmagic.dashboard.users.prefix'),
        'namespace' => '\Webmagic\Users\Http\Controllers',
        'middleware' => config('webmagic.dashboard.users.middleware'),
        'as' => 'users_module::'
    ], function(){

    /*
     * Routes for users in dashboard
     */
    Route::group([
        'prefix' => 'user',
        'as' => 'users::'
        ], function () {
        Route::get('/', [
            'as' => 'index',
            'uses' => 'UserController@index'
        ]);

        Route::post('/', [
            'as' => 'store',
            'uses' => 'UserController@store'
        ]);

        Route::get('create', [
            'as' => 'create',
            'uses' => 'UserController@create'
        ]);

        Route::get('{user_id}/edit', [
            'as' => 'edit',
            'uses' => 'UserController@edit'
        ])->where('user_id', '[0-9]+');

        Route::put('{user_id}', [
            'as' => 'update',
            'uses' => 'UserController@update'
        ])->where('user_id', '[0-9]+');

        Route::post('{user_id}/destroy', [
            'as' => 'destroy',
            'uses' => 'UserController@destroy'
        ])->where('user_id', '[0-9]+');
    });

    /*
     * Routes for permissions in dashboard
     */
    Route::group([
        'prefix' => 'permissions',
        'as' => 'permissions::'
    ], function () {
        Route::get('/', [
            'as' => 'index',
            'uses' => 'PermissionController@index'
        ]);

        Route::post('/', [
            'as' => 'store',
            'uses' => 'PermissionController@store'
        ]);

        Route::get('create', [
            'as' => 'create',
            'uses' => 'PermissionController@create'
        ]);

        Route::get('{permission_id}/edit', [
            'as' => 'edit',
            'uses' => 'PermissionController@edit'
        ])->where('permission_id', '[0-9]+');

        Route::put('{permission_id}', [
            'as' => 'update',
            'uses' => 'PermissionController@update'
        ])->where('permission_id', '[0-9]+');

        Route::post('{permission_id}/destroy', [
            'as' => 'destroy',
            'uses' => 'PermissionController@destroy'
        ])->where('permission_id', '[0-9]+');
    });

    /*
     * Routes for roles in dashboard
     */
    Route::group([
        'prefix' => 'roles',
        'as' => 'roles::'
    ], function () {
        Route::get('/', [
            'sa' => 'index',
            'uses' => 'RoleController@index'
        ]);

        Route::post('/', [
            'as' => 'store',
            'uses' => 'RoleController@store'
        ]);

        Route::get('create', [
            'as' => 'create',
            'uses' => 'RoleController@create'
        ]);

        Route::get('{role_id}/edit', [
            'as' => 'edit',
            'uses' => 'RoleController@edit'
        ])->where('role_id', '[0-9]+');

        Route::put('{role_id}', [
            'as' => 'update',
            'uses' => 'RoleController@update'
        ])->where('role_id', '[0-9]+');

        Route::post('{role_id}/destroy', [
            'as' => 'destroy',
            'uses' => 'RoleController@destroy'
        ])->where('role_id', '[0-9]+');
    });
});
