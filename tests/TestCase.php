<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Webmagic\Dashboard\DashboardServiceProvider;
use Webmagic\Users\UserServiceProvider;


class TestCase extends BaseTestCase
{
    /**
     * Boots the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../vendor/laravel/laravel/bootstrap/app.php';
        $app->make(Kernel::class)->bootstrap();

        $app->register(DashboardServiceProvider::class);
        $app->register(UserServiceProvider::class);

        // Load factories for tests
        $customPath = __DIR__.'/../src/database/factories/';

        $app->singleton(\Illuminate\Database\Eloquent\Factory::class, function ($app) use ($customPath) {
            $faker = $app->make(\Faker\Generator::class);

            return \Illuminate\Database\Eloquent\Factory::construct($faker, $customPath);
        });

        return $app;
    }

    /**
     * Setup DB before each test.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->app['config']->set('database.default','sqlite');
        $this->app['config']->set('database.connections.sqlite.database', ':memory:');
//        Factory::$pathToFactories = '../../../src/database/factories';

        $this->artisan('migrate', ['--path' => '../../../src/database/migrations']);
    }
}