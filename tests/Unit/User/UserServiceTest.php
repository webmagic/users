<?php

namespace Tests\Unit\Role;


use Tests\TestCase;
use Webmagic\Users\RoleService;
use Webmagic\Users\UserService;

class UserServiceTest extends TestCase
{
    protected $repo;


    /**
     * Init
     */
    public function setUp()
    {
        parent::setUp();

        $this->repo = new UserService();
    }


    /**
     * Get for select
     */
    public function testForSelect()
    {
        factory(\Webmagic\Users\Models\Role::class, 3)->create();

        $roles = $this->repo->getRolesForSelect();


        $this->assertTrue(count($roles) == 3);
    }


    /**
     * Get all
     */
    public function testGetAll()
    {
        factory(\Webmagic\Users\Models\User::class, 3)->create();

        $users = $this->repo->getAll();


        $this->assertTrue($users->count() == 3);
    }

    /**
     * Create
     */
    public function testCreate()
    {
        // create permissions for role
        $permission = factory(\Webmagic\Users\Models\Permission::class)->create();
        $role = factory(\Webmagic\Users\Models\Role::class)->create();

        $name = 'Test name';
        // store user
        $this->repo->create([
            'name' => $name,
            'password' => 'pass'
        ]);

        // get stored user
        $user = $this->repo->getUserByID(1);
        // add role & permission
        $user->attachRole($role);
        $user->attachPermission($permission);


        $this->assertTrue($user['name'] == $name);
        $this->assertTrue($user->roles->count() == 1);
        $this->assertTrue($user->permissions->count() == 1);
    }


    /**
     * Update
     */
    public function testUpdate()
    {
        $user = factory(\Webmagic\Users\Models\User::class)->create();

        $name = 'New name';
        $this->repo->updateUser($user['id'], [
            'name' => $name
        ]);

        $updatedUser = $this->repo->getUserByID(1);


        $this->assertTrue($updatedUser['name'] == $name);
    }


    /**
     * Destroy
     */
    public function testDestroy()
    {
        // create role with permissions
        factory(\Webmagic\Users\Models\User::class)->create();

        // destroy
        $this->repo->removeUser(1);
        // try get destroyed
        $destroyed_role = $this->repo->getUserByID(1);


        $this->assertTrue($destroyed_role == null);
    }
}





