<?php

namespace Tests\Unit\Permission;


use Tests\TestCase;
use Webmagic\Users\PermissionService;

class PermissionServiceTest extends TestCase
{
    protected $repo;


    /**
     * Init
     */
    public function setUp()
    {
        parent::setUp();

        $this->repo = new PermissionService();
    }


    /**
     * Create
     */
    public function testCreate()
    {
        $slug = 'test-name';

        $permission = $this->repo->create([
            'name' => 'Test name',
            'slug' => $slug
        ]);

        // get stored permissions
        $check_permission = $this->repo->getByID($permission->id);

        $this->assertTrue($check_permission['slug'] == $slug);
    }


    /**
     * Update
     */
    public function testUpdate()
    {
        // create
        $name = 'Test name';
        $permission = factory(\Webmagic\Users\Models\Permission::class)->create();
        // update
        $this->repo->update($permission->id, ['name' => $name]);
        // get updated
        $updated_permission = $this->repo->getByID($permission->id);

        $this->assertTrue($updated_permission['name'] == $name);
    }

    /**
     * Destroy
     */
    public function testDestroy()
    {
        // create
        $permission = factory(\Webmagic\Users\Models\Permission::class)->create();
        // destroy
        $this->repo->destroy($permission->id);
        // try get destroyed
        $destroyed_permission = $this->repo->getByID($permission->id);

        $this->assertTrue($destroyed_permission == null);
    }

    /**
     * Get for select
     */
    public function testForSelect()
    {
        factory(\Webmagic\Users\Models\Permission::class, 3)->create();

        $permissions = $this->repo->getPermissionForSelect();

        $this->assertTrue(count($permissions) == 3);
    }


    /**
     * Get all
     */
    public function testGetAll()
    {
        factory(\Webmagic\Users\Models\Permission::class, 3)->create();

        $permissions = $this->repo->getAll();

        $this->assertTrue($permissions->count() == 3);
    }
}





