<?php

namespace Tests\Unit\Role;


use Tests\TestCase;
use Webmagic\Users\RoleService;

class RoleServiceTest extends TestCase
{
    protected $repo;


    /**
     * Init
     */
    public function setUp()
    {
        parent::setUp();

        $this->repo = new RoleService();
    }


    /**
     * Get for select
     */
    public function testForSelect()
    {
        factory(\Webmagic\Users\Models\Role::class, 3)->create();

        $roles = $this->repo->getRolesForSelect();

        $this->assertTrue(count($roles) == 3);
    }


    /**
     * Get all
     */
    public function testGetAll()
    {
        factory(\Webmagic\Users\Models\Role::class, 3)->create();

        $roles = $this->repo->getAll();

        $this->assertTrue($roles->count() == 3);
    }

    /**
     * Create
     */
    public function testCreate()
    {
        // create permissions for role
        $permissions = factory(\Webmagic\Users\Models\Permission::class, 6)->create();
        // prepare for store
        $tmp = [];
        foreach ($permissions as $item){
            $tmp[$item['slug']] = rand(0,1);
        }
        $permissions = $tmp;

        // create role
        $slug = 'test-slug';
        $this->repo->create([
            'name' => 'Test name',
            'slug' => $slug,
            'permissions' => $permissions
        ]);

        // get stored role
        $checkRole = $this->repo->getByID(1);

        $this->assertTrue($checkRole['slug'] == $slug);
    }


    /**
     * Update
     */
    public function testUpdate()
    {
        // create role with permissions
        factory(\Webmagic\Users\Models\Role::class)
            ->create()
            ->each(function ($item) {
                $permissions = factory(\Webmagic\Users\Models\Permission::class, 3)->create();
                foreach ($permissions as $permission){
                    $item->permissions()->save($permission);
                }
            });

        // get role
        $roleByID = $this->repo->getById(1);
        $role = $this->repo->getRoleWithPermissions($roleByID['slug']);


        // prepare permissions for store
        $tmp = [];
        foreach ($role[0]['permissions'] as $item){
            $tmp[$item['slug']] = 0;
        }
        $permissions = $tmp;

        // update role
        $name = 'Test name';
        $this->repo->update(
            $role[0]['id'],
            [
                'name' => $name,
                'permissions' => $permissions
            ]
        );

        // get updated role
        $checkRole = $this->repo->getRoleWithPermissions($roleByID['slug']);

        $this->assertTrue($checkRole[0]['name'] == $name);
        $this->assertTrue($checkRole[0]['permissions'] == null);
    }


    /**
     * Destroy
     */
    public function testDestroy()
    {
        // create role with permissions
        factory(\Webmagic\Users\Models\Role::class)
            ->create()
            ->each(function ($item) {
                $permissions = factory(\Webmagic\Users\Models\Permission::class, 3)->create();
                foreach ($permissions as $permission){
                    $item->permissions()->save($permission);
                }
            });

        // destroy
        $this->repo->destroy(1);
        // try get destroyed
        $destroyed_role = $this->repo->getByID(1);

        $this->assertTrue($destroyed_role == null);
    }
}





